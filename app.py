import numpy as np
from flask import Flask, request
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask import jsonify
from utils import get_predictions
import matplotlib.pyplot as plt

import random

app = Flask(__name__)

photos = UploadSet('photos', IMAGES)

app.config['UPLOADED_PHOTOS_DEST'] = 'static/img'

app.config['UPLOAD_FOLDER'] = 'static/img'
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

configure_uploads(app, photos)

@app.route("/")
def hello():
    return "api / path"

@app.route("/classify", methods=["GET","POST"])
def classify():
    if request.method == 'POST' and 'photo' in request.files:
    
        image = request.files['photo']
        image.save("tmp/teste")

#        with open("tmp/teste", "rb") as imageFile:
#            f = imageFile.read()
#            b = bytearray(f)

#            print(b)
#            print()
#        print("==============================================")
        #matplot_img = plt.imread("tmp/teste")

        # chama rede neural
#        classes = ['healthy','defect']
#        class_ = random.choice(classes)
        classs = get_predictions("tmp/teste")
        np_class = classs.data.cpu().numpy()
        print("---------------------")
	
        np_exp = np.exp(np_class)
        print(np_exp)
        if(np.argmax(np_exp)==0):
            classe = "defect"
        else:
            classe = "healthy"
        data = {"file_name": image.filename, "class": classe}

        return jsonify(data)

    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=photo>
      <input type=submit value=Upload>
    </form>
    '''

if __name__ == '__main__':
    app.run(debug=True, host= '0.0.0.0', port=8888)
