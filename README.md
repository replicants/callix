# Callix

Tratamento, análise e inteligência em API do Eden Project voltado para frutos, específicamente, maçãs.

[Dataset bruto](https://zenodo.org/record/1313615#.W6ZNK9hKjBI)

[Preprocessamento das imagens](https://gitlab.com/replicants/callix/blob/master/img-preprocessing.ipynb)