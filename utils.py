import torch
from torch import nn, cuda, backends, FloatTensor, LongTensor, optim
from torch.autograd import Variable
import torch.nn.functional as F
from torchvision.models import resnet34
from fastai.conv_learner import *
from fastai.model import *
import torchvision.transforms as transforms

def load_model():

    the_model = torch.load('/home/paperspace/fastai/courses/dl1/processed/models/224_all.pt')
    state_dict = torch.load("/home/paperspace/fastai/courses/dl1/processed/models/224_all.h5")
#    state_dict = torch.load("/home/paperspace/fastai/courses/dl1/processed/models/224_all_final.h5")
    the_model.load_state_dict(state_dict)
    the_model.precompute=True
    return the_model

def get_predictions(img_path, nms=True):

    #preprocessing ?

    model  = load_model()

    image = image_loader(img_path)
    model.eval()
    #return  model(image[:,1:,:,:])
    return model(image)

imsize = 256
loader = transforms.Compose([transforms.Resize(imsize), transforms.ToTensor()])

def image_loader(image_name):
    """load image, returns cuda tensor"""
    image = Image.open(image_name)
    image = loader(image).float()
    image = Variable(image, requires_grad=True)
    image = image.unsqueeze(0)  #this is for VGG, may not be needed for ResNet
    return image.cuda()  #assumes that you're using GPU


